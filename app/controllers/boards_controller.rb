class BoardsController < ApplicationController
  def show
    board = User.find_by(username: params[:username]).board
    render json: board.as_json(include:[:suggestions]), status: :ok
  end
end
