# Suggestible back-end

## Dev env setup

Install gems

```
bundle
```

Create postgres role with name `suggestible` and password `password`. Make sure they can create databases.

Create, migrate, and seed db

```
rake db:setup
```

### Creating an env file

This app uses a `.env` file to store env vars. You'll have to create one of your own since it's in the `.gitignore`

```
touch .env
```

Only env var you need (for now) is `FRONTEND_URL`. This is the URL of whatever your frontend server is. So, for example, if I am running the built frontend using sinatra on port `9292` then I need set my `.env` file to:

```
FRONTEND_URL=localhost:9292
```



## Techs

- Ruby 2.5.0
- Rails 5.1.6
- pg

