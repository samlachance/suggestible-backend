Rails.application.routes.draw do
  post 'authenticate', to: 'authentication#authenticate'
  resources :boards, param: :username, path: 'u'
end
