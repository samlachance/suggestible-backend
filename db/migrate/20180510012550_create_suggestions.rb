class CreateSuggestions < ActiveRecord::Migration[5.1]
  def change
    create_table :suggestions do |t|
      t.integer :author_id
      t.references :board, foreign_key: true
      t.string :body
      t.text :description

      t.timestamps
    end
  end
end
